'use strict';

var torredelpriorApp = angular.module('torredelpriorApp', [])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/history', {
        templateUrl: 'views/history.html',
        controller: 'HistoryCtrl'
      })
      .when('/oferim', {
        templateUrl: 'views/oferim.html',
        controller: 'OferimCtrl'
      })
      .when('/espais', {
        templateUrl: 'views/espais.html',
        controller: 'EspaisCtrl'
      })
      .when('/entorn', {
        templateUrl: 'views/entorn.html',
        controller: 'EntornCtrl'
      })
      .when('/tarifes', {
        templateUrl: 'views/tarifes.html',
        controller: 'TarifesCtrl'
      })
      .when('/reserves', {
        templateUrl: 'views/reserves.html',
        controller: 'ReservesCtrl'
      })
      .when('/contacte', {
        templateUrl: 'views/contacte.html',
        controller: 'ContacteCtrl'
      })
      .when('/om-som', {
        templateUrl: 'views/om-som.html',
        controller: 'Om-SomCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
