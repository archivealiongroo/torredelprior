'use strict';

torredelpriorApp.directive('slimbox', function() {
  return {
    templateUrl: 'templates/slimbox.html',
    restrict: 'E',
    link: function ($element, attributes) {

    	var photo = {
    		'id':attributes[0].attributes.getNamedItem("id").value,
    		'img':
    			{
    			'src':attributes[0].attributes.getNamedItem("src-image").value,
    			'alt':attributes[0].attributes.getNamedItem("alt-image").value
    			},
    		'leyend':attributes[0].attributes.getNamedItem("leyend-image").value
    	}
		
		var tag = 'slimbox#' + photo.id;
	
		/* Add images attributes */
		$(tag).find('img').each(function(i){
			$(this).attr(photo.img);
		});

		/* Add anchor attributes */
		$(tag).find('a').each(function(i){
			$(this).attr('href', photo.img.src);			
		});		

    }
  };
});
