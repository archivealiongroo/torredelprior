'use strict';

torredelpriorApp.directive('sidemenu', function() {
  return {
    templateUrl: 'templates/sidemenu.html',
    restrict: 'E',
    link: function ($scope, $element, attributes) {

		$('#bmenu li').on('a', 'click', function(){
	        $(this).toggleClass("bmenu-activo");
	    });
	}

  };
});
