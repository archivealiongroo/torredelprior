'use strict';

torredelpriorApp.directive('sidelogo', function() {
  return {
    templateUrl: 'templates/sidelogo.html',
    restrict: 'E'
  };
});
