'use strict';

torredelpriorApp.directive('header', function() {
  return {    
    templateUrl: 'templates/header.html',
    restrict: 'E'    
  };
});
