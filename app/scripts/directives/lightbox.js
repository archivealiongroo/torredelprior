'use strict';

torredelpriorApp.directive('lightbox', function() {
  return {
    templateUrl: 'templates/lightbox.html',
    restrict: 'E',
    link: function($element, attributes) {

    	var photo = 
	    		{
		    		'id':attributes[0].attributes.getNamedItem("id").value,
		    		'img':
		    			{
		    			'src':attributes[0].attributes.getNamedItem("src-image").value,
		    			'alt':attributes[0].attributes.getNamedItem("alt-image").value
		    			},
		    		'leyend':attributes[0].attributes.getNamedItem("leyend-image").value
		    	},
		
		 	tag = 'lightbox#' + photo.id,
		 
			modal = 
				{
					'html': '',
					'id': 'modal-'+photo.id
				}
	
		/* Add image attributes */
		$(tag).find('img').each(function(i){
			$(this).attr(photo.img);
		});		
		$(tag).find('.photo-leyend').text(photo.leyend);
		
		/* Preparing modal div's */
		$(tag).find('#modal-link').attr('data-reveal-id', modal.id);
		$(tag).find('#modal').attr('id', modal.id);
		
		/*
		$('div#'+modal.id).imgLiquid({
	        fill: true,
	        fadeInTime: 200,
	        horizontalAlign: "center",
	        verticalAlign: "top"
	    });	
		*/

		modal.html = $('div#'+modal.id)[0];
				
		$(tag).find('div#'+modal.id).remove();
		$(modal.html).appendTo($('div#global-modal'));
	
    }
  };
});
