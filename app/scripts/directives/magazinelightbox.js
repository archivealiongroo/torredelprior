'use strict';

torredelpriorApp.directive('magazinelightbox', function() {
  return {
    templateUrl: 'templates/magazinelightbox.html',
    restrict: 'E'
  };
});
