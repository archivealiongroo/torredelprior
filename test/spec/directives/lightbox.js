'use strict';

describe('Directive: lightbox', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<lightbox></lightbox>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the lightbox directive');
  }));
});
