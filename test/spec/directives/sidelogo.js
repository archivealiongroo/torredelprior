'use strict';

describe('Directive: sidelogo', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<sidelogo></sidelogo>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the sidelogo directive');
  }));
});
