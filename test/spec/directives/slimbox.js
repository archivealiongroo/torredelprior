'use strict';

describe('Directive: slimbox', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<slimbox></slimbox>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the slimbox directive');
  }));
});
