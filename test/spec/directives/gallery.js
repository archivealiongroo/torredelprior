'use strict';

describe('Directive: gallery', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<gallery></gallery>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the gallery directive');
  }));
});
