'use strict';

describe('Directive: magazinelightbox', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<magazinelightbox></magazinelightbox>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the magazinelightbox directive');
  }));
});
