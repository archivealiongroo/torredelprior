'use strict';

describe('Directive: sidemenu', function() {
  beforeEach(module('torredelpriorApp'));

  var element;

  it('should make hidden element visible', inject(function($rootScope, $compile) {
    element = angular.element('<sidemenu></sidemenu>');
    element = $compile(element)($rootScope);
    expect(element.text()).toBe('this is the sidemenu directive');
  }));
});
