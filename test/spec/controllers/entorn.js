'use strict';

describe('Controller: EntornCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var EntornCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    EntornCtrl = $controller('EntornCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
