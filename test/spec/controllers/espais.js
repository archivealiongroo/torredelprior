'use strict';

describe('Controller: EspaisCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var EspaisCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    EspaisCtrl = $controller('EspaisCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
