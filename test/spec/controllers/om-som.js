'use strict';

describe('Controller: Om-SomCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var Om-SomCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    Om-SomCtrl = $controller('Om-SomCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
