'use strict';

describe('Controller: ContacteCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var ContacteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    ContacteCtrl = $controller('ContacteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
