'use strict';

describe('Controller: TarifesCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var TarifesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    TarifesCtrl = $controller('TarifesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
