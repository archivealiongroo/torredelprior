'use strict';

describe('Controller: OferimCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var OferimCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    OferimCtrl = $controller('OferimCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
