'use strict';

describe('Controller: ReservesCtrl', function() {

  // load the controller's module
  beforeEach(module('torredelpriorApp'));

  var ReservesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    scope = {};
    ReservesCtrl = $controller('ReservesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function() {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
